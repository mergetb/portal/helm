---
- name: Portal Application Observability 
  hosts: localhost

  tasks:
    # Install loki, promtail, and optionally garfana to follow all logs in the cluster
    # including k8s and portal services. 
    - name: Create Loki namespace
      when: loki.enabled == true
      kubernetes.core.k8s:
        name: "{{ loki.namespace }}"
        api_version: v1
        kind: Namespace
        state: present

    - name: Insall Loki/Grafana helm repository
      when: loki.enabled == true
      kubernetes.core.helm_repository:
        name: grafana
        repo_url: "https://grafana.github.io/helm-charts"

    # Note: this loki in monolithic mode. If we need more performance, we can move to 
    # simple scaled mode. In simple scaled, we can horizonally expand readers and writers. We
    # may need to revist storage backing though. 
    - name: Install Loki via helm (in monolithic mode)
      when: loki.enabled == true
      kubernetes.core.helm:
        name: "{{ loki.helm_name.loki }}"
        create_namespace: true
        namespace: "{{ loki.namespace }}"
        chart_ref: grafana/loki
        # These values are tweaked from the deafult "monolithic" values here:
        # https://github.com/grafana/loki/blob/main/production/helm/loki/single-binary-values.yaml
        # In the future we may just want to pull this file in and use it directly.
        values:
          loki:
            commonConfig:
              replication_factor: 1
            schemaConfig:
              configs:
                - from: 2024-04-01
                  store: tsdb
                  object_store: s3
                  schema: v13
                  index:
                    prefix: loki_index_
                    period: 24h
            ingester:
              chunk_encoding: snappy
            tracing:
              enabled: true
            querier:
              # Default is 4, if you have enough memory and CPU you can increase, reduce if OOMing
              max_concurrent: 2
          deploymentMode: SingleBinary
          singleBinary:
            replicas: 1
            resources:
              limits:
                cpu: 3
                memory: 4Gi
              requests:
                cpu: 2
                memory: 2Gi
            extraEnv:
              # Keep a little bit lower than memory limits
              - name: GOMEMLIMIT
                value: 3750MiB
          
          chunksCache:
            # default is 500MB, with limited memory keep this smaller
            writebackSizeLimit: 10MB
          
          # Enable minio for storage
          minio:
            enabled: true
          
          # Zero out replica counts of other deployment modes
          backend:
            replicas: 0
          read:
            replicas: 0
          write:
            replicas: 0
          
          ingester:
            replicas: 0
          querier:
            replicas: 0
          queryFrontend:
            replicas: 0
          queryScheduler:
            replicas: 0
          distributor:
            replicas: 0
          compactor:
            replicas: 0
          indexGateway:
            replicas: 0
          bloomCompactor:
            replicas: 0
          bloomGateway:
            replicas: 0

    - name: Install promtail to read logs
      when: loki.enabled == true
      kubernetes.core.helm:
        name: "{{ loki.helm_name.promtail }}"
        chart_ref: grafana/promtail
        namespace: "{{ loki.namespace }}"
        values:
          config:
            clients:
            - url: "http://loki-gateway/loki/api/v1/push"  # TODO: parameterize this by namespace/helm-name
              tenant_id: "{{ loki.tenant_id }}"

    - name: Install grafana if requested
      when: loki.grafana.enabled == true and loki.enabled == true
      kubernetes.core.helm:
        name: "{{ loki.grafana.name }}"
        chart_ref: grafana/grafana
        namespace: "{{ loki.grafana.namespace }}"
        values:
          persistence:
            type: pvc
            enabled: true
            storageClass: "{{ loki.grafana.storageClass }}"


    # TODO figure out how to add loki as a data source to grafana

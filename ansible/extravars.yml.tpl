tolerations:
  svc:
    tolerations:
      - effect: NoSchedule
        key: dedicated
        operator: Equal
        value: service_worker
  wrk:
    tolerations:
      - effect: NoSchedule
        key: dedicated
        operator: Equal
        value: service_worker

# Local host updates (for portal appliance install only)
local:
    resolve: true
    applianceaddr: $HOST_IP

# k8s namespace
ns: $NAMESPACE

#
# Auth settings.
# 
auth:
  dbpass: $AUTHDB_PASS
  postgres_user: $AUTHDB_USER
  postgres_db: $AUTHDB_DB
  dbname: data-auth-db-0
  kratos_cookie: $AUTH_COOKIE
  kratos_cipher: $AUTH_CIPHER
  appname: "auth"
  chartref: $AUTH_CHARTREF
  registration_webhook: $REGISTER_WEBHOOK
  courier_from: $COURIER_FROM
  courier_name: $COURIER_NAME
  courier_uri: $COURIER_URI
  oidc_methods:
    # uncomment to add support for google and gitlab logins.
    # will need to create own id and secrets for new installations.
    #   config:
    #     providers:
    #       # google
    #       - client_id: 752880253173-29ab6i8e9718ertk3absqdkurk6qupnb.apps.googleusercontent.com
    #         client_secret: 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
    #       # gitlab
    #       - client_id: 8023378cabf1de3c79d7d8cf3bf7d8150edf73ca1a8d9d92be00a8c4c08298ed
    #         client_secret: 966c14a50c240d376171b8f4b802564eae05a863e6fcb1dfc0ca64d64fbb8500


#
# Smallstep CA
#
stepca:
  capw: $STEPCA_PW
  provpw: $STEPPROV_PW
  caname: step-ca
  provisioner: ops@mergetb.net
  appname: stepca

#
# Merge Portal installation specific variables.
#
portal:
  copy_existing: $COPY_EXISTING 
  appname: portal
  fqdn: $PORTAL_FQDN
  repository: $PORTAL_REPO
  pullPolicy: Always
  tag: $PORTAL_TAG
  opsId: $OPSID
  opsEmail: $OPSEMAIL
  opspw: $OPSPW
  miniopw: $MINIOPW
  artifact_miniopw: $ARTIFACT_MINIOPW
  # This port must be unique across all portal installs in the cluster.
  # default value is 36000
  wgdport: $WGDPORT
  chartref: $PORTAL_CHARTREF

  communications:
    enabled: $COMMUNICATIONS_ENABLED
    smtpHost: $SMTP_HOST
    smtpPort: $SMTP_PORT
    smtpUser: $SMTP_USER
    smtpPassword: $SMTP_PW
    portalEMailFrom: $PORTAL_EMAIL_FROM

  realize:
    expiration_check_duration: $RLZ_EXPIRATION_CHECK_LOOP
    expiration_warning_times: $RLZ_EXPIRATION_WARN_TIMES

#
# Launch
#
launch:
  repository: $LAUNCH_REPO
  pullPolicy: Always
  tag: $LAUNCH_TAG
  chartref: $LAUNCH_CHARTREF

  # if adding a redirect from an old url, add these values
  redirect_from_fqdn: launch.mergetb.example.net
  redirect_to_fqdn: testlaunch.net

#
# Storage config
#
storage:
    path: /var/vol/$NAMESPACE/storage
    server: master1

#
# Information about persistent volumes in the cluster.
#
# (These can be created using volumes.yml if needed.)
# 
pvs:
  stepca:
    name: "$NAMESPACE-stepca"
    storage: 5Gi
    volmode: Filesystem
    accessmode: ReadWriteOnce
  etcd:
    name: data-etcd-0
    storage: 10Gi
    accessmode: ReadWriteOnce
  minio:
    storage: 10Gi
    accessmode: ReadWriteOnce
  artifact_minio:
    storage: 10Gi
    accessmode: ReadWriteOnce
  authdb:
    storage: 10Gi
  mergefs:
    name: "$NAMESPACE-mergefs"
    storage: 10Gi
    volmode: Filesystem
    accessmode: ReadWriteMany
    path: "/var/vol/$NAMESPACE/mergefs"
    hostname: master1
  mergetfsxdc:
    name: "$NAMESPACE-mergefs-xdc"  # NB: must match mergefs path
    storage: 10Gi
    volmode: Filesystem
    accessmode: ReadWriteMany
    path: "/var/vol/$NAMESPACE/mergefs"
    hostname: master1
  git:
    storage: 10Gi
    volmode: Filesystem
    accessmode: ReadWriteOnce

#
# Logging configuration.
#
loki:
  enabled: $LOKI_ENABLED
  namespace: loki
  helm_name:
    loki: loki 
    promtail: promtail 
  tenant_id: merge
  grafana:
    enabled: $GRAFANA_ENABLED
    name: grafana
    namespace: loki
    storageClass: default

#!/usr/bin/env bash

trap 'echo ERROR' err

function pw {
    # nanosecond should be enough. 
    # we could put a small sleep in here if needed.
    date +%N | sha256sum | base64 | head -c 32; echo
}

# passwords
export AUTHDB_PASS=${AUTHDB_PASS:=$(pw)}
export AUTHDB_USER=${AUTHDB_USER:-postgres}
export AUTHDB_DB=${AUTHDB_DB:-db}
export AUTH_COOKIE=${AUTH_COOKIE:=$(pw)}
export AUTH_CIPHER=${AUTH_CIPHER:-$(pw)}
export STEPCA_PW=${STEPCA_PW:-$(pw)}
export STEPPROV_PW=${STEPPROV_PW:-$(pw)}
export OPSPW=${OPSPW:-$(pw)}
export MINIOPW=${MINIOPW:-$(pw)}
export ARTIFACT_MINIOPW=${ARTIFACT_MINIOPW:-$(pw)}
export COURIER_FROM=${COURIER_FROM:-"ops@mergetb.example.net"}
export COURIER_NAME=${COURIER_NAME:-"Merge Testbed OPs"}
export COURIER_URI=${COURIER_URI:-"'smtp://name:pass@postfix-mail:587?skip_ssl_verify=true'"}

export NAMESPACE=${NAMESPACE:-merge}
export PORTAL_FQDN=${PORTAL_FQDN:-mergetb.example.net}
export PORTAL_TAG=${PORTAL_TAG:=v1.1.4-helm}
export LAUNCH_TAG=${LAUNCH_TAG:=v1.1.2-helm}
export OPSID=${OPSID:-portalops}
export OPSEMAIL=${OPSEMAIL:-ops@$PORTAL_FQDN}
export WGDPORT=$(( $RANDOM % 512 + 36000 ))

# for internal repo use: export REGISTRY=host.internal:5000
export REGISTRY=${REGISTRY:-registry.gitlab.com}
export PORTAL_REPO=${PORTAL_REPO:-$REGISTRY/mergetb/portal/services}
export LAUNCH_REPO=${LAUNCH_REPO:-$REGISTRY/mergetb/portal/launch}

export REGISTER_WEBHOOK=${REGISTER_WEBHOOK:-http://127.0.0.1/not-a-hook}

export STEPCA_PROV=${STEPCA_PROV:-ops@$PORTAL_FQDN}

export COPY_EXISTING=${COPY_EXISTING:-false}

# appliance install only
export HOST_IP=${HOST_IP:-192.168.126.10}

# helm 
# you can set these to full path on local machine to test updates
export PORTAL_CHARTREF=${PORTAL_CHARTREF:-mergetb/merge-portal}
export LAUNCH_CHARTREF=${LAUNCH_CHARTREF:-mergetb/merge-launch}
export AUTH_CHARTREF=${AUTH_CHARTREF:-mergetb/merge-auth}

# loki & logging observability
export LOKI_ENABLED=${LOKI_ENABLED:-true}
export GRAFANA_ENABLED=${GRAFANA_ENABLED:-true}

# comms
export COMMUNICATIONS_ENABLED=${COMMUNICATIONS_ENABLED:-false}
export SMTP_HOST=${SMTP_HOST}  # required if comms enabled
export SMTP_PORT=${SMTP_PORT}  # required if comms enabled
export SMTP_USER=${SMTP_USER}
export SMTP_PASSWORD=${SMTP_PASSWORD}
export PORTAL_EMAIL_FROM=${PORTAL_EMAIL_FROM}  # required if comms enabled

# realization expiration
export RLZ_EXPIRATION_CHECK_LOOP=${RLZ_EXP_CHECK_LOOP}  # helm default: 5m
export RLZ_EXPIRATION_WARN_TIMES=${RLZ_EXPIRATION_WARN_TIMES} # helm default "72h, 24h, 1h"

envsubst > extravars.yml

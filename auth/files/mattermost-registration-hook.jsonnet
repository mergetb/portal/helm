function(ctx) {
  "text": ctx.identity.traits.username + " has created a new account via " + std.split(ctx.flow.ui.action, "/")[2] + ".\n@here please init and activate the account."
}

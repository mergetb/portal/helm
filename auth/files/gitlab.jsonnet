local claims = {
  email_verified: false,
} + std.extVar('claims');
{
  identity: {
    traits: {
      [if 'email' in claims then 'email']: claims.email,
      username: claims.nickname,
      [if 'picture' in claims then 'picture']: claims.picture,
      [if 'name' in claims then 'name']: claims.name,
      [if 'profile' in claims then 'profile']: claims.profile
    },
  },
}

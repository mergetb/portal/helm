#!/usr/bin/bash

. ./env

helm uninstall -n $ns $appname
kubectl delete pv $pvname

echo Now log onto the master node and rm -rf the data in the postgres DB in the persistent-volume.
echo If you do not do this, a database re-install on the same PV will not get updated pw/data.

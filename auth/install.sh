#!/usr/bin/bash 

#
# Generate secrets and passwords.
#
date +%N | sha256sum | base64 | head -c 32 > dbpass.txt
date +%N | sha256sum | base64 | head -c 32 > kratos-secret.txt
date +%N | sha256sum | base64 | head -c 32 > kratos-secret-cipher.txt

. ./env

# env-secret is not checked in to the repo and holds secrets and per-install 
# keys and ids. Used to storre OIDC client ids and secrets.
. ./env-secret

#
# helm does not support variables in values.yaml thus this poor workaround
# This way we have a single values.yaml but can make variables easy to read/set 
# via an env file. Note: this kinda defeats the purpose of helm. 
# yq eval -i ".primary.persistence.selector.matchLabels.name = \"${pvname}\"" postgresql-values.yaml

#
# This install assumes that the user ui for auth runs on the launch domain under the /auth path.
# It assumes that the launch helm chart will setup TLS and the ingress.
#
dbpass=$(cat dbpass.txt)
authPath=auth
launchURL=https://$launchFQDN
allowedDomain="https://*.${domain}"
authURL=$launchURL/$authPath
action=${action:-install}  # action=upgrade helm ... to upgrade.
storageClass=csi-cephfs-sc
postgresClaim=auth-pvc
postgresUsername=admin
postgresPassword=thisisapassword

helm -n $ns $action \
    --create-namespace \
    --set postgresql.global.storageClass=$storageClass \
    --set postgresql.primary.persistence.existingClaim=$postgresClaim \
    --set postgresql.global.postgres.auth.postgresPassword=$postgresPassword \
    --set postgresql.global.postgres.auth.username=$postgresUsername \
    --set postgresql.global.postgres.auth.password=$postgresPassword \
    --set postgresql.global.postgres.auth.database=$dbpass \
    --set kratos.kratos.secrets.cookie=$(cat kratos-secret.txt) \
    --set kratos.kratos.secrets.cipher=$(cat kratos-secret-cipher.txt) \
    --set kratos.kratos.config.dsn="postgres://postgres:${dbpass}@${appname}-postgresql.${ns}.svc.cluster.local:5432/db" \
    --set kratos.kratos.config.log.level=debug \
    --set kratos.kratos.config.session.cookie.domain=$domain \
    --set kratos.kratos.config.selfservice.default_browser_return_url=$launchURL \
    --set kratos.kratos.config.selfservice.allowed_return_urls[0]=$launchURL \
    --set kratos.kratos.config.selfservice.flows.error.ui_url=$authURL/error \
    --set kratos.kratos.config.selfservice.flows.settings.ui_url=$authURL/settings \
    --set kratos.kratos.config.selfservice.flows.recovery.ui_url=$authURL/recovery \
    --set kratos.kratos.config.selfservice.flows.verification.ui_url=$authURL/verification \
    --set kratos.kratos.config.selfservice.flows.verification.after.default_browser_return_url=$launchURL \
    --set kratos.kratos.config.selfservice.flows.logout.after.default_browser_return_url=$authURL/login \
    --set kratos.kratos.config.selfservice.flows.login.ui_url=$authURL/login \
    --set kratos.kratos.config.selfservice.flows.registration.ui_url=$authURL/registration \
    --set kratos.kratos.config.selfservice.methods.oidc.config.providers[0].client_id=$googleClientId \
    --set kratos.kratos.config.selfservice.methods.oidc.config.providers[0].client_secret=$googleClientSecret \
    --set kratos.kratos.config.selfservice.methods.oidc.config.providers[1].client_id=$gitlabClientId \
    --set kratos.kratos.config.selfservice.methods.oidc.config.providers[1].client_secret=$gitlabClientSecret \
    --set kratos.ingress.public.hosts[0].host=$launchFQDN \
    --set kratos.ingress.admin.hosts[0].host=$launchFQDN \
    --set kratos.kratos.config.serve.public.base_url=$authURL \
    --set kratos.kratos.config.serve.public.cors.allowed_origins[0]=$allowedDomain \
    -f values.yaml \
    $appname .

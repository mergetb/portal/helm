#!/usr/bin/bash

. ./env

# host path 0 = launch
# host path 1 = auth API

helm -n $ns install \
   --set apiUri=$apiURI \
   --set authUri=$authURI \
   --set launchUri=$launchURI \
   -f values.yaml \
   $appname .

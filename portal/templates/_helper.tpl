{{/*
Expand the name of the chart.
*/}}
{{- define "portal.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "portal.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "portal.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "portal.labels" -}}
helm.sh/chart: {{ include "portal.chart" . }}
{{ include "portal.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}


{{/*
Selector labels
*/}}
{{- define "portal.selectorLabels" -}}
app.kubernetes.io/name: {{ include "portal.name" . }}
app.kubernetes.io/instance: {{ $.Release.Name }}
{{- end }}

{{/*
Component Selector
*/}}
{{- define "portal.componentLabels" -}}
{{- if .component -}}
app.kubernetes.io/component: {{ .component }}
{{- end -}}
{{- end }}

{{/*
XDC Namespace deployment env fields.
*/}}
{{- define "portal.namespacesEnv" -}}
- name: MERGE_NAMESPACE
  value: {{ .Release.Namespace }}
- name: XDC_NAMESPACE
  value: {{ include "portal.xdcNamespace" . }}
{{- end }}

{{/* 
Standard Portal ENV for deployments.
*/}}
{{- define "portal.Env" -}}
- name: LOGLEVEL
  value: {{ .Values.logging.level }}
- name: LOGFORMAT
  value: {{ .Values.logging.format }}
{{- end }}

{{/*
MINIO env fields.
*/}}
{{- define "portal.minioEnv" -}}
- name: MINIO_ROOT_USER
  valueFrom:
    secretKeyRef:
      name: minio
      key: root-user
      optional: false
- name: MINIO_ROOT_PASSWORD
  valueFrom:
    secretKeyRef:
      name: minio
      key: root-password
      optional: false
{{- end }}

{{/*
Email env fields.
*/}}
{{- define "communications.emailEnv" -}}
- name: SMTP_ENABLED
  value: {{ .Values.communications.enabled | quote }}
- name: SMTP_HOST
  value: {{ .Values.communications.smtpHost | default "localhost" | quote }}
- name: SMTP_PORT
  value: {{ .Values.communications.smtpPort | default "587" | quote }}
{{- if .Values.communications.smtpUser }}
- name: SMTP_USER
  value: {{ .Values.communications.smtpUser | quote }}
{{- end }}
{{- if .Values.communications.smtpPassword }}
- name: SMTP_PW
  value: {{ .Values.communications.smtpPassword | quote }}
{{- end }}
{{- end }}

{{/*
ARTIFACT MINIO env fields.
*/}}
{{- define "portal.artifact_minioEnv" -}}
{{- if (index .Values "artifact-minio").enabled -}}
- name: ARTIFACT_MINIO_ROOT_USER
  valueFrom:
    secretKeyRef:
      name: artifact-minio
      key: root-user
      optional: false
- name: ARTIFACT_MINIO_ROOT_PASSWORD
  valueFrom:
    secretKeyRef:
      name: artifact-minio
      key: root-password
      optional: false
{{- end  -}}
{{- end }}

{{/*
etcd db volume mounts
*/}}
{{- define "portal.etcdVolMounts" -}}
# - name: etcdb-cert-volume
#   mountPath: /dbcerts
{{- end }}

{{/*
step-ca cert volume mounts
*/}}
{{- define "portal.stepcaVolMounts" -}}
- name: step-ca-certs
  mountPath: /etc/step-ca/data/certs
  readOnly: true
{{- end }}

{{/*
step-ca cert volumes
*/}}
{{- define "portal.stepcaVols" -}}
{{/* hack workaround for dash in identiier. See https://stackoverflow.com/questions/63853679/helm-templating-doesnt-let-me-use-dash-in-names */}}
{{- $sca := (index .Values "step-certificates") }}
- name: step-ca-certs
  configMap:
    defaultMode: 420
    name: "{{- $sca.releaseName -}}-step-certificates-certs"
{{- end }}

{{/*
XDC namespace
*/}}
{{- define "portal.xdcNamespace" }}
{{- if .Values.xdcNamespace }} 
{{- .Values.xdcNamespace }}
{{- else }}
{{- .Release.Namespace }}-xdc
{{- end }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "portal.serviceAccountName" -}}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "portal.xdcServiceAccountName" -}}
{{- default "xdc-controller" .Values.xdcServiceAccount.name }}
{{- end }}

{{/*
certificate issue name is based on release. 
*/}}
{{- define "portal.certIssuer" }}
{{- .Values.certIssuer | default (print .Release.Name "-" .Release.Namespace "-selfsigned-issuer") }}
{{- end }}

{{/*
Debug - dump values.
Use to stop template generation and dump the given value.
Examples:
* {{- template "portal.var_dump" .Chart }}
* {{- template "portal.var_dump" . }}
* {{- template "portal.var_dump" $myVar }}
*/}}
{{- define "portal.var_dump" -}}
{{- . | mustToPrettyJson | printf "\nThe JSON output of the dumped var is: \n%s" | fail }}
{{- end -}}


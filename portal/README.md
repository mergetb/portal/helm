# Merge Portal

## Requirements before installation

Merge Portal Helm chart as a few prereqs. These are listed below.

Apply taints to service nodes and xdc nodes.

example:
```
kubectl taint node master1 dedicated=service_worker:NoSchedule
```

node `master1` can now schedule service_worker pods (all portal services).

Nodes that host XDCs should have the `xdc_worker` taint.

example:
```
kubectl taint node xw0 dedicated=xdc_worker:NoSchedule
```

The idea here is to spawn XDCs on nodes dedicated to hosting XDCs only and
keep the portal services on other nodes. This is only a suggestion though.
The help chart supports adding tolerations or not.

### Persistent Volumes

Each Merge Portal installation requires 6 persistent volumes:

|NAME       | USE                        | SIZE |
| --------- | -------------------------- | ---- |
|auth       | identity data              | small |
|etcd       | etcd portal data           | medium |
|git        | user git repos             | medium/small |
|mergefs    | user project and home dirs | large |
|mergefs-xdc| user project and home dirs | large |
|minio      | portal data                | large/medium |
|stepca     | ssh keys and cert CA data  | small |

These can be named anything, but these names must be added to the helm values.yml or given on the command line when
installing a merge portal via helm. The names also must be unique across the kubernetes cluster as persistent volumes
are not namespaced. A sample file which generates these persistent volumes as host mounts is in ./examples/pv.yaml

NOTE: the `mergefs` and `mergefs-xdc` persistent volumes *must* point to the same underlying data. The data written
to this volume is used in two namespaces. Each namespace requires its own persistent volume to read the data.

### Ingress Certificates

If you want to use self-signed certs for the Merge Portal ingresses, then
install certificate manager: see https://cert-manager.io/docs/installation/helm/

helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.8.0 --set installCRDs=true

If this is a live deployment, set the appropriage cert-manager annotations in values.yaml.

NOTE: This needs to be done even though the cert-manger is included in the Portal chart as a dependency.
There appears to be a bug in cert-manager (or maybe helm/k8s in general) such that the CRDs need to exist
before helm can process a template using custom resources (and "certificates" and "clusterissuers" are CRDs)
in the cert-manager. In theory, setting cert-manager as a dependency on the portal helm would install theses. In
practice this does not seem to be the case.

## Install Smallstep step-certifcates

Install step-certificates. This is the CA used for SSH keys and certs in the Portal.
https://github.com/smallstep/helm-charts/tree/master/step-certificates

The helm chart should be able to support loading this a dependent chart, but due to how smallstep
generates default values, this is difficult. So the step-certificates helm chart must be installed
as a separate step before the Merge Portal is installed. This needs to be installed in the same
root namespace as the Merge Portal.

Step-ca uses the `step` binaray to generate helm values. Download and install that. Or run it in
a container. Then generate your values as shown below.

A few things to note:
* The "first provisioner" you create will be passed to the Merge Portal install, so keep track of it.
* The DNS added to the CA must start with the name of the step-ca installation. (`helm install [name]...`). So if you install
under the name `step-ca`, the DNS names you add will be:
  `step-ca-step-certificates,step-ca-step-certificates.default.svc.cluster.local,127.0.0.1`
* In order to cleanly base64 encode the step-ca password it is recommended that you create
your own password instead of letting `step` choose for you. This is to avoid reserved shell
chars.
* The name of your step-certificates installation needs to be added to your Merge Portal helm install values.

Example setup:
```
$ echo -n thisismypassword > pass.txt
$ step ca init \
  --ssh \
  --helm \
  --deployment-type=standalone \
  --name step-ca \
  --dns step-ca-step-certificates,step-ca-step-certificates.default.svc.cluster.local,127.0.0.1 \
  --address :9000 \
  --provisioner=ops@mergetb.net \
  --password-file=pass.txt \
  --provisioner-password-file=pass.txt \
  > step-values.yaml

Generating root certificate... done!
Generating intermediate certificate... done!
Generating user and host SSH certificate signing keys... done!
````

Base64 encode the password. Note: be careful not to include a trailing newline when you create the password
file. Use `echo -n ...`.  If the installed system does not generate SSH certs on login, this is the first thing
to check.

```
$ cat pass.txt | base64 >> pass.b64
```

Edit the step-values.yaml and set the following things:

* The existing volume name for the step-ca database
* tolerations, if using

```
$ cat >> step-values.yaml
ca:
  db:
    existingClaim: mergetb-stepca

tolerations:
  - effect: NoSchedule
    key: dedicated
    operator: Equal
    value: xdc_worker
^D
```

There is a sample step-values.yaml in the `examples` directory.

Now install it:
```
helm install \
  -n $namespace \
  -f step-values.yaml \
  --set inject.secrets.ca_password=$(cat pass.b64) \
  --set inject.secrets.provisioner_password=$(cat pass.b64) \
  step-ca smallstep/step-certificates --create-namespace
```

Note that the stateful set for the CA will not start as it's waiting on the persistent volume claim to exist.
The Merge Portal chart will create this.

# Merge Portal Images

Setup portal images to be loaded. If needed. Default is to pull from registry.gitlab.com. If you are
loading from the pre-built images on gitlab, there is nothing that needs to be done for this step.

Example of loading into a local portal appliance

cd $portal-repository
export REGISTRY="mergetb.example.net:5000"
export REGISTRY_PATH="mergetb/portal/services"
ymk build.yml
ymk containers.yml
ymk push-containers.yml

Edit values.yaml and set global.image.repository: "localhost:5000/mergetb/portal/services"
(Or run the install with --set 'global.image.repository=localhost:5000/mergetb/portal/services'

## Installation

Install the portal. Choose a unique namespace. Note that if you do nothing,
this will create 2 namespaces:

* $namespace
* $namespace-xdc

You must also pass the minio database password on the command line. You can use `$(date +%N | sha256sum | base64 | head -c 32; echo)`
to generate a password.

Install the helm repo for the merge-portal: `helm repo add mergetb https://gitlab.com/api/v4/projects/36949361/packages/helm/stable`
helm install
Download the default values.yaml for the merge-portal helm-chart. Edit to fit your installation.

```
helm show values mergetb/merge-portal > values.yaml
```

(A minimal values file can be found in the gitlab repository `examples` directory)

```
$ export namespace=merge
$ date +%N | sha256sum | base64 | head -c 32 > minio-pass.txt
$ date +%N | sha256sum | base64 | head -c 32 > auth-db-pass.txt

$ helm install \
    --namespace $namespace \
    -f values.yaml \
    --set minio.auth.rootPassword=$(cat minio-pass.txt) \
    --set auth.db.password=$(cat auth-db-pass.txt) \
    myportal mergetb/merge-portal
```

The install will take a short time to complete. All components will come up. The final step is creating the initial ops identity account. This will
be attemped a nubmer of times and the installation will only be complete when this jobs completes.

-------------------------------------------------

Confirm things look ok.

```
kubectl -n $namespace get all
kubectl -n $namespace-xdc get all
```

Confirm the persistent volumes are claimed:

```
kubectl get pv
```

Look for things that did not start. The `etcd` pod seems to be the last to start up. This can be checked with
`kubectl -n namespace get statefulsets/etcd`. If everything looks ok, attempt to login to the ops account:

```
mrg config set server [grpc server name]
mrg login ops --nokeys -p $OPSPW
mrg list ids
```

## Merge Portal Configuration

After the portal is running there is basic configuration needed before it can be used. At least one facility needs to be added to the portal
for any resources to be available. And resource `pools` need to be configured.

### Adding a facility

See documentation at https://next.mergetb.org

Here is an example of adding a facility:

```
$ mrg login phops
Enter password for phops:
$ mrg new fac phobos api.phobos.example.com model/cmd/phobos.xir merge-install/apiserver.pem
$ mrg show fac phobos
Name: phobos
Address: api.phobos.example.com
Access Mode: Public
Members: phops (Creator/Active)
Certificate: true
```

### Configurating Resource Pools

When experiments are realized the portal reads a list of available resources from a pool that the experiment's project belongs to.
If the project does not belong to a specific pool, the `default` pool is used. By default projects belong to no pools. So a `default`
pool should be created and at least some resources added to it.  The creator of the pool becomes the admin of the pool. Resources from
facilities are then added by facility adminstrators.

Create the `default` pool:

* `mrg login [account] -p [password]`
* `mrg new pool default`

As a facility adminstrator, add the facility to the pool along with some resources. (`r0 r1 ... rN` is a list of resources in the facility in this example):

* `mrg login [facility operator] -p [password]`
* `mrg pool add facility default [facility name] [r0 r1 r2 ... rN]`

Confirm the pool contains resources: `mrg show pool default`

Here is an example of creating the `default` pool:

```
$ mrg login glawler
Enter password for glawler:
$ mrg new pool default
$ mrg list pools
Name       Creator    Facilities    Projects    Resources    Description
----       -------    ----------    --------    ---------    -----------
default    glawler                              0
$ mrg logout
$ mrg login phops
Enter password for phops:
$ mrg list facilities
Name      Address                   Description    Mode
----      -------                   -----------    ----
phobos    api.phobos.example.com                   Public
$ mrg show facility phobos
Name: phobos
Address: api.phobos.example.com
Access Mode: Public
Members: phops (Creator/Active)
Certificate: true
$ mrg pool add facility default phobos x0 x1 x2 x3
$ mrg list pools
Name       Creator    Facilities    Projects    Resources    Description
----       -------    ----------    --------    ---------    -----------
default    glawler    phobos                    4
$ mrg show pool default
Name: default
Description:
Creator: glawler
Projects:
Resources:
        phobos: x0 x1 x2 x3
```


## Parameters

Here is an example of a minimal values.yaml file to install a Merge Portal. This example installs into a FDQN of `*.lh.example.net` and the
namespaces `lh`, and `lh-xdc`. ("lh" is short for "lighthouse", an instance of a Merge Portal.)

```
image:
  repository: "host.internal:5000/mergetb/portal/services"
  pullPolicy: Always
  tag: latest

opsId: ops@example.net
opsPw: NTFmYzk2ZTM0YWQ5ZTQ4NjJhYmZkZTkx

apiserver:
  ingress:
    api:
      endpoint: api.lh.example.net
      allowedOrigins: "https://*.lh.example.net,https://localhost:9000"
    grpc:
      endpoint: grpc.lh.example.net

cred:
  certProvisioner: ops@mergetb.net

gitserver:
  ingress:
    endpoint: git.lh.example.net

sshJump:
  fqdn: jump.lh.example.net

xdc:
  hostAliases: "192.168.126.10,grpc.lh.example.net,api.lh.example.net,git.lh.example.net"
  xdcDomain: xdc.lh.example.net

etcd:
  persistence:
    selector:
      matchLabels:
        name: lh-etcd

step-certificates:
  releaseName: step-ca
  ca:
    db:
      existingClaim: lh-stepca
      size: 10Gi
      accessModes:
        - ReadWriteOnce

minio:
  auth:
    rootPassword: Y2Y0MDAwZWUzZTU4ODI2NjI2NWY4ZWQ0
```

Note that all passwords can (and likely should) be passed in via `--set` on the `helm install` command line.

### Image Repository

The Merge Portal is made up of a handful of cooperating services. These services are loaded from images in an image repository.
The repository URL for each component is made up of four pieces: a base URL, a repository path, a component name, and an image tag.
The format is `baseurl/repository path/compoent name:image tag`.
For example, the URL for the `ssh-jump` component may be:

```
gitlab.com/mergetb/portal/services/xdc/ssh-jump:v1.0.5
```

Where
* base url - `gitlab.com/mergetb/portal/services`
* path - `xdc`
* component - `ssh-jump`
* tag - `v1.0.5`

(For a default installation the `repository path` for most components is empty.)

| Name                  | Description                                     | Default Value                              |
| ----------------------| ----------------------------------------------- | ---------------------------------- |
| `image.repository`    | Base URL for image repositiory                  | `registry.gitlab.com/mergetb/portal/services/merge` |
| `image.pullPolicy`    | Pull Policy for images                          | `IfNotPresent`                       |
| `image.tag`           | Image tag                                       | `$Chart.AppVersion`                  |

## Kubernetes Settings

These are applied to the service components of the portal to limit on which nodes they are run.

| Name                            | Description                                     | Default Value                      |
| --------------------------------| ----------------------------------------------- | ---------------------------------- |
| `xdcNamespace`                  | k8s namespace for XDC components                | `{{ Release.Namespace }}-xdc`      |
| `authNamespace`                 | k8s namespace for authentication/id components  | `{{ Release.Namespace  }}-auth`    |
| `serviceTolerations`            | Limit portal services to tainted nodes.         | `- effect: NoSchedule`             |
|                                 |                                                 | `  key: dedicated    `             |
|                                 |                                                 | `  operator: Equal   `             |
| `xdcTolerations`                | Limit XDCs to tainted nodes.                    | `- effect: NoSchedule`             |
|                                 |                                                 | `  key: dedicated    `             |
|                                 |                                                 | `  operator: Equal   `             |
|                                 |                                                 | `  value: service_worker`          |
| `serviceAccount.annotations`    | Service account annotations.                    | `{}`                               |
| `serviceAccount.name`           | Service account name.                           | `""`                               |


## Merge Portal Configuration

| Name                                    | Description                                     | Default Value                              |
| ----------------------------------------| ----------------------------------------------- | ---------------------------------- |
| `certIssuer`                | If given use as the cert issuer ("letsencrypt") otherwise use self signed cert. | not set |
| `opsId`                                 | The username for the initial `OPs` account      | "" |
| `opsPw`                                 | The password for the ops account                | "" |
| `apiserver.component`                   | Name of the API server component | `apiserver` |
| `apiserver.repository_path`             | Image path for `apiserver` component |                                    |
| `apiserver.ingress.api.endpoint`        | The FQDN of the Merge API endpoint |                                    |
| `apiserver.ingress.api.allowedOrigins`  | The allowed origins that can access the Merge API endpoint |                                    |
| `apiserver.ingress.api.className`       | Ingress Class Name | `nginx` |
| `apiserver.ingress.api.tlsSecret`       | The Merge API ingress TLS secret name | `""` |
| `apiserver.ingress.api.annotations`     | Annotations for the Merge API Ingress. (cert-manager annotations can be added here)| `""` |
| `apiserver.ingress.grpc.endpoint`       | The FQDN of the Merge GRPC endpoint | |
| `apiserver.ingress.grpc.className`      | Ingress Class Name | `nginx` |
| `apiserver.ingress.grpc.tlsSecret`      | The Merge GRPC ingress TLS secret name | `""` |
| `apiserver.ingress.grpc.annotations`    | Annotations for the Merge GRPC Ingress. (cert-manager annotations can be added here if `useSelfSignedIngresses` is not `true`)) | `""` |
| `commission.component`                  | Name of the commission component | `commission` |
| `commission.repository_path`            | The `commission` component repository path | `""` |
| `cred.component`                        | Name of the credentials component | `cred` |
| `cred.repository_path`                  | The `cred` component repository path | `""` |
| `cred.certProvisioner`                  | The name of the SSH CA certifcate provisioner to use from the step-ca installation | `ops@mergetb.net` |
| `gitserver.component`                   | Name of the git server component | `gitserver` |
| `gitserver.repository_path`             | The `gitserver` component repository path | |
| `gitserver.persistence.volumeStorage`   | How much space to request from k8s for git repositories storage | `10Gi`|
| `gitserver.persistence.volumeName`      | The name of the persistent storage volume to use for the git repositories | `{{ .Release.Namespace }}-git }} `|
| `gitserver.ingress.className`           | The ingress class name for the git ingress | `nginx` |
| `gitserver.ingress.endpoint`            | The ingress endpoint for the git servive | `""` |
| `gitserver.ingress.annotations`         | Annotations for the git ingress. (cert-manager annotations can be added here if `useSelfSignedIngresses` is not `true`) | `""` |
| `identity.component`                    | Name of the identity service component | `identity` |
| `identity.repository_path`              | The `identity` component repository path | |
| `materialize.component`                 | Name of the materialization component | `materialize` |
| `materialize.repository_path`           | The `materialize` component repository path | |
| `mergefs.component`                     | Name of the Merge Filesystem component | `mergefs` |
| `mergefs.repository_path`               | The `mergefs` component repository path | |
| `mergefs.persistence.volumeStorage`     | How much space to request from k8s for mergefs storage | `10Gi` |
| `mergefs.persistence.volumeName`        | The name of the persistent storage volume to use for the Merge FS | `{{ .Release.Namespace }}-mergefs }} `|
| `model.component`                       | Name of the model component | `model` |
| `model.repository_path`                 | The `model` component repository path | |
| `realize.component`                     | Name of the realization component | `realize` |
| `realize.repository_path`               | The `realize` component repository path | |
| `sshJump.component`                     | Name of the ssh jump container | `ssh-jump` |
| `sshJump.repository_path`               | The `ssh-jump` component repository path | |
| `sshJump.persistence.volumeStorage`     | This must match the `mergefs` storage size. See above for details | `10Gi` |
| `sshJump.persistence.volumeName`        | This must match the `mergefs` volume name. See above for details | `10Gi` |
| `sshJump.service.sshPort`               | External port for ssh access to the Merge XDCs | `2022` |
| `sshJump.service.xdcdPort`              | Internal port to connect to XDC services | `22` |
| `sshJump.service.externalip`            | The IP from which the jump service will get ssh traffic | `192.168.126.10` |
| `wgsvc.component`                       | Name of the Wireguard Server component | `wgsvc` |
| `wgsvc.repository_path`                 | The `wgsvc` component repository path | |
| `xdc.component`                         | Name of the XDC Operator component | `xdc` |
| `xdc.repository_path`                   | The `xdc` operator component repository path | |
| `xdc.hostAliases`                       | Host aliases to add to spawned XDCs. These should be the FQDNs to Merge Portal services that will be accessed from XDCs | `192.168.126.10,grpc.mergetb.example.net,api.mergetb.example.net,git.mergetb.example.net` |
| `xdc.xdcDomain`                         | Root domain for spawned XDCs. Web interfaces for XDCS will be put here | `""` |
| `xdc.xdcImage`                          | The full path to the default XDC base image. This is the image used when spawning new XDCs if no custom image path is given | `""` |
| `xdc.xdcTag`                            | The tag of the xdc image to spawn | `Chart.appVersion` |
| `xdc.wgdPort`                           | The port on which to connect to the wgd running on the node. This must be unique across Merge Portal installations on this k8s cluster | `36000` |
| `wgd.component`                         | Name of the Wireguard Daemon component | `wgd` |
| `wgd.repository_path`                   | The `wgd` component repository path | `xdc/wgd` |
| `wgd.containerControlEndpoint`          | Control endpoint for `wgd` to use to access the container manager | `unix:///var/run/crio/crio.sock` |
| `wgd.containerControlSocket`            | Control socket used to access container manager | `/var/run/crio/crio.sock` |
| `wgd.netNsPath`                         | Path to access network namespace on the host machine | `/var/run/netns` |

## Minio / etcd

The Merge portal uses both minio and etcd databases. These settings are passed directly to the helm chart for minio and etcd and are documented here for
[minio](https://github.com/bitnami/charts/tree/master/bitnami/minio/) and [etcd](https://github.com/bitnami/charts/tree/master/bitnami/etcd/). The default
values are set for a typical Merge Portal install. Note that etcd is run without authentication and relies on secured ingresses and k8s namespace for security.

| Name                            | Description                                     | Value                              |
| --------------------------------| ----------------------------------------------- | ---------------------------------- |
| `minio.auth.rootPassword`       | admin password. should be set on command line | |
| `minio.persistence.enabled`     | If `true` use persistent storage | `false` |
| `minio.existingClaim` | An existing persistent storage claim for minio use | |
| `minio.tolerations` | Tolerations for minio database pods | As above for Merge Portal `service_worker`s |
| `etcd.auth.rbac.create` | Create RBAC auth for etcd | `false` |
| `etcd.auth.rbac.allowNoneAuthentication` | If `true` disable auth for etcd | `true` |
| `etcd.persistence.enabled` | Enable persistent storage | `true` |
| `etcd.persistence.storageClass` | Storage class | `-` Default install uses named persistent volume |
| `etcd.size` | Amount of storage requestsed for etcd from k8s | `10Gi` |
| `etcd.selector.matchLabels.name` | Name of the existing persistent volume in the k8s cluster for etcd storage | |

